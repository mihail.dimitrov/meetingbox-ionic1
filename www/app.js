// Ionic Starter App
angular.module('appMain', [
    'ionic',
    'features',
    'settings'
])

.run(function($ionicPlatform, App, SetUpBrowser) {
    $ionicPlatform.ready(function() {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            cordova.plugins.Keyboard.disableScroll(true);

        }
        if (window.StatusBar) {
            // org.apache.cordova.statusbar required :::
            StatusBar.styleDefault();
        }

        if (location.href.indexOf('meeting') > -1) {
            // Browser environment:
            console.log('Browser Environment.');

            // App.setEventSettingsUrl('https://vanilla.test.meeting-box.com/mobile/mobile-api.php?eventId=147');
            App.setEventSettingsUrl('http://dev.succevo.com/my-meetingbox.com/mobile/mobile-api.php?eventId=1084')
            App.setEventID(1084);

            SetUpBrowser.init();
        } else {
            // Cordova environment:
            console.log('Cordova Environment.');

            App.setEventSettingsUrl('https://vanilla.test.meeting-box.com/mobile/mobile-api.php?eventId=77');


        }

    });

    $ionicPlatform.on('resume', function() {
        console.log('--- APP RESUME ---');

    });
});