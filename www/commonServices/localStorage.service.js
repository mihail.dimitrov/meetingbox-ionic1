angular.module('settings')
.factory('LocalStorage', [
    '$window',
    function (
        $window) {
        
        function get(key, defaultValue) { return $window.localStorage[key] || defaultValue || null; }
        function set(key, value) { $window.localStorage[key] = value; }

        function getObject(key, defaultValue) { 
            if($window.localStorage[key] != undefined){
                return JSON.parse($window.localStorage[key]);
            }else{
              return defaultValue || null;
            }
        }
        function setObject(key, value) { $window.localStorage[key] = JSON.stringify(value); }

        function remove(key) { $window.localStorage.removeItem(key); }
        function clear() { $window.localStorage.clear(); }
        
        return {
            get: get,
            set: set,
            getObject: getObject,
            setObject: setObject,
            remove: remove,
            clear: clear
        };
    }
]);