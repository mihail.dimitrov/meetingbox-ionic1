angular.module('features')
    .config(function($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('app.accountsAttendees', {
                url: '/accounts/attendees/:id',
                params: {
                    id: ""
                },
                views: {
                    'menuContent': {
                        templateUrl: function($stateParams) {
                            return 'features/accounts/attendees.html';
                        },
                        controller: 'AttendeesCtrl'
                    }
                }
            })
    })
    .controller('AttendeesCtrl', function($scope) {
        console.log('AttendeesCtrl');



    })