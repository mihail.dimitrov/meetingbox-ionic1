angular.module('features')
    .config(function($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('app.accountsExhibitors', {
                url: '/accounts/exhibitors/:id',
                params: {
                    id: ""
                },
                views: {
                    'menuContent': {
                        templateUrl: function($stateParams) {
                            return 'features/accounts/ehxibitors.html';
                        },
                        controller: 'ExhibitorsCtrl'
                    }
                }
            })
    })
    .controller('ExhibitorsCtrl', function($scope) {
        console.log('ExhibitorsCtrl');



    })