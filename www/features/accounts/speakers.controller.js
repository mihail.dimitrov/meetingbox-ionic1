angular.module('features')
    .config(function($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('app.accountsSpeakers', {
                url: '/accounts/speakers/:id',
                params: {
                    id: ""
                },
                views: {
                    'menuContent': {
                        templateUrl: function($stateParams) {
                            return 'features/accounts/speakers.html';
                        },
                        controller: 'SpeakersCtrl'
                    }
                }
            })
    })
    .controller('SpeakersCtrl', function($scope) {
        console.log('SpeakersCtrl');



    })