angular.module('features')
    .config(function($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('app.accountsSponsors', {
                url: '/accounts/sponsors/:id',
                params: {
                    id: ""
                },
                views: {
                    'menuContent': {
                        templateUrl: function($stateParams) {
                            return 'features/accounts/sponsors.html';
                        },
                        controller: 'SponsorsCtrl'
                    }
                }
            })
    })
    .controller('SponsorsCtrl', function($scope) {
        console.log('SponsorsCtrl');



    })