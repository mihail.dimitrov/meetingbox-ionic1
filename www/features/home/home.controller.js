angular.module('features')
    .config(function($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('app.home', {
                url: '/home',
                views: {
                    'menuContent': {
                        templateUrl: 'features/home/home.html',
                        controller: 'HomeCtrl'
                    }
                }
            })
    })
    .controller('HomeCtrl', function($scope) {
        console.log('HomeCtrl');
        
    })