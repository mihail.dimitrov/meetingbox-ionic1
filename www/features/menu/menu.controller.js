angular
    .module("features", ["settings"])
    .config(function($stateProvider, $urlRouterProvider) {
        $stateProvider.state("app", {
            url: "/app",
            abstract: true,
            templateUrl: "features/menu/menu.html",
            controller: "MenuCtrl"
        });
        $urlRouterProvider.otherwise("/");
    })
    .controller("MenuCtrl", function($scope, App, AppFunctions) {
        console.log("MenuCtrl");

        $scope.menuItems = App.getEventMenuItemsArr();

    });