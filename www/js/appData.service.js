angular.module("settings")
    .factory("MeetingboxDB", [
        "$q",
        "$http",
        "$rootScope",
        "$templateCache",
        "App",
        function($q, $http, $rootScope, $templateCache, App) {

            function getEventSetings() {
                var dfd = jQuery.Deferred();
                $http.get(App.getEventSettingsUrl()).then(function(response) {
                    App.setEventSettingsData(response.data.Events[App.getEventID()]);
                    dfd.resolve(true);
                }, function(error) {
                    dfd.resolve(error);
                });
                return dfd.promise();
            }

            return {
                getEventSetings: getEventSetings
            };
        }
    ])
    .factory("IndexedDB", [
        "$q",
        "$rootScope",
        "$templateCache",
        "App",
        function($q, $rootScope, $templateCache, App) {


            return {

            };
        }
    ]);