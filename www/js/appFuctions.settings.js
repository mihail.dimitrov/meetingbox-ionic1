angular.module("settings").factory("AppFunctions", [
    "$q",
    "$rootScope",
    "$templateCache",
    "$state",
    "LocalStorage",
    "App",
    function($q, $rootScope, $templateCache, $state, LocalStorage, App) {

        function setUpEventSettings() {
            var areTherePassProtectedFeatures = false,
                prevUserId,
                eventSettingsData = App.getEventSettingsData(),
                firstPageToLoad = 'app.home';
            eventId = App.getEventID();

            if (eventSettingsData.has_login == '0') {
                // Check for pass protected features:
                for (var moduleIndex in eventSettingsData.MenuItems) {
                    if (eventSettingsData.MenuItems[moduleIndex].password_protected === 1 && areTherePassProtectedFeatures === false) {
                        areTherePassProtectedFeatures = true;
                        break;
                    }
                }

                if (areTherePassProtectedFeatures) {
                    // There is no login for the current event but there are pass protected features:
                    if (LocalStorage.get('is_user_logged', false)) {
                        App.setUserID(LocalStorage.get('user_id'));
                        App.setLoggedUserInfo(LocalStorage.get('logged_user_data'))
                    } else {

                        if (LocalStorage.get('user_id') == null) {
                            LocalStorage.set('user_id', eventSettingsData.user_id);
                            App.setUserID(eventSettingsData.user_id);
                        } else {
                            App.setUserID(LocalStorage.get('user_id'));
                        }
                    }
                } else {
                    // There are no pass protected features for the current event:
                    if (LocalStorage.get('user_id') == null) {
                        LocalStorage.set('user_id', eventSettingsData.user_id);
                        App.setUserID(eventSettingsData.user_id);
                    } else {
                        App.setUserID(LocalStorage.get('user_id'));
                    }
                }
            }

            if (eventSettingsData.has_login == '1') {
                // There is login for the current event:
                if (LocalStorage.get('is_user_logged', false)) {
                    // The user is logged:
                    App.setUserID(LocalStorage.get('user_id'));
                    App.setLoggedUserInfo(LocalStorage.get('logged_user_data'))
                } else {
                    // TODO:
                    // The user is not logged. Go to login page.

                }
            }

            buildEventMenu().then(function(success) {
                loadEvent(firstPageToLoad);
            }, function(err) {

            });
            // Get all modules data:
            getEventModulesData();
        }

        // Build Event Menu:
        function buildEventMenu() {
            console.log('Building Event Menu......');
            var dfd = jQuery.Deferred();
            var menuItems = App.getEventSettingsData().MenuItems,
                generatedMenuItems = [];

            for (let i = 0; i < menuItems.length; i++) {
                //   console.log(menuItems[i]);

                var moduleObject = {
                    moduleType: menuItems[i].module.toLowerCase().replace(/\s/, ""),
                    subModuleType: '',
                    moduleId: menuItems[i].id,
                    templateId: '',
                    moduleState: 'app.home()',
                    moduleExternalLink: '',
                    currentMenuItem: '',
                    icon: menuItems[i].fa_icon,
                    isActiveGrid: menuItems[i].on_grid,
                    isPrivate: menuItems[i].password_protected,
                    title: menuItems[i].title
                };

                if (menuItems[i].module == "accounts" || (
                        menuItems[i].module == "speakers" ||
                        menuItems[i].module == "attendees" ||
                        menuItems[i].module == "sponsors" ||
                        menuItems[i].module == "exhibitors"
                    )) {

                    if (menuItems[i].module == "accounts" && (menuItems[i].submodule == undefined || menuItems[i].id == 0)) {
                        continue;
                    } else {
                        var curtrentModule = (menuItems[i].module == "accounts" ? menuItems[i].submodule.toLowerCase().replace(/\s/, "") : menuItems[i].module);

                        // moduleObject.subModuleType = menuItems[i].submodule.toLowerCase().replace(/\s/, "");
                        // moduleObject.templateId = "accounts-" + menuItems[i].submodule.toLowerCase().replace(/\s/, "") + '-' + menuItems[i].id + '.html';

                        switch (curtrentModule) {
                            case 'speakers':
                                moduleObject.moduleState = 'app.accountsSpeakers({id:' + menuItems[i].id + '})';
                                break;
                            case 'attendees':
                                moduleObject.moduleState = 'app.accountsAttendees({id:' + menuItems[i].id + '})';
                                break;
                            case 'sponsors':
                                moduleObject.moduleState = 'app.accountsSponsors({id:' + menuItems[i].id + '})';
                                break;
                            case 'exhibitors':
                                moduleObject.moduleState = 'app.accountsExhibitors({id:' + menuItems[i].id + '})';
                                break;
                            default:
                                break;
                        }
                    }
                } else if (menuItems[i].module == "schedule") {
                    // moduleObject.moduleState = '';
                    // moduleObject.templateId = "schedule-" + menuItems[i].id;

                } else if (menuItems[i].module == "pages" || menuItems[i].module == "forms" || menuItems[i].module == "livepoll") {
                    // moduleObject.moduleState = '';
                    // moduleObject.templateId = menuItems[i] + "-" + menuItems[i].id;

                } else if (menuItems[i].module == "floorplans") {
                    // moduleObject.moduleState = '';
                    // moduleObject.templateId = moduleObject.moduleType + "-" + menuItems[i].id;

                } else if (menuItems[i].module == "map") {
                    // moduleObject.moduleState = '';
                    moduleObject.moduleId = menuItems[i].mobile_id
                        // moduleObject.templateId = moduleObject.moduleType;

                } else if (menuItems[i].module == "externallink") {
                    // moduleObject.moduleState = '';
                    moduleObject.moduleExternalLink = menuItems[i].link;
                    // moduleObject.templateId = "";

                } else {
                    // moduleObject.moduleState = '';
                    moduleObject.moduleId = '';
                    // moduleObject.templateId = "";
                }
                generatedMenuItems.push(moduleObject);
            }

            App.setEventMenuItemsArr(generatedMenuItems);
            dfd.resolve(true);
            return dfd.promise();
        }

        // function createModuleTemplate(moduleObj) {
        //     console.log('--- CREATE MODULE TEMPLATE ---');
        //     console.log(moduleObj);
        //     var templateHtml = '';
        //     // templateHtml += '<script type="text/ng-template" id="' + moduleObj.templateId + '">';
        //     templateHtml += '<ion-view view-title="' + moduleObj.title + '"><ion-content><h1>' + moduleObj.title + '</h1></ion-content></ion-view>';
        //     // templateHtml += '<p>This is the content of the template</p>';
        //     // templateHtml += '</script>';
        //     $templateCache.put(moduleObj.templateId, templateHtml);
        //     // $('body').append(templateHtml);
        // }

        function getEventModulesData() {
            var eventModules = App.getEventSettingsData().MenuItems;

            for (var eventModule in eventModules) {
                // Check the type of the module:
                if (eventModules[eventModule].module !== 'menudivider' &&
                    eventModules[eventModule].module !== 'myschedule' &&
                    eventModules[eventModule].module !== 'mybookmarks' &&
                    eventModules[eventModule].module !== 'mynotes' &&
                    eventModules[eventModule].module !== 'externallink' &&
                    eventModules[eventModule].module !== 'publicchat' &&
                    eventModules[eventModule].module !== 'activityfeed') {
                    // Check if the data of the current module has to be downloaded:
                    if (!App.isModuleAddedIntoEventModulesArr(eventModules[eventModule].module)) {
                        // Call function to get modules data versions:
                        // downloadModuleDataVersion(eventModules[eventModule].module);

                        console.log('Going to get/download data for module ' + eventModules[eventModule].module);

                        App.addNewModuleIntoEventModulesArr(eventModules[eventModule].module);
                    } else {
                        // The data for this module has been already got it.
                    }
                } else {
                    // We don't have to get anything for this module.
                }
            }

        }

        function loadEvent(pageToLoad) {
            console.log('pageToLoad: ', pageToLoad);

            $state.go(pageToLoad);
        }



        return {
            setUpEventSettings: setUpEventSettings
        };
    }
]);