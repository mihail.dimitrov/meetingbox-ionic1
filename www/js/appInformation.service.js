angular.module("settings").factory("App", [
    "$q",
    "$rootScope",
    function($q, $rootScope) {
        var eventSettingsUrl = "",
            eventSettingsData,
            eventMenuItems = [],
            eventID,
            userID,
            loggedUserInfo,
            allEventModules = [];

        // Event Settings URL:
        function getEventMenuItemsArr() {
            return eventMenuItems;
        }

        function setEventMenuItemsArr(newArr) {
            if (newArr) {
                eventMenuItems = newArr;
                // TODO:
                // Save into indexedDB and/or localstorage.
            }
        }


        // Event Settings URL:
        function getEventSettingsUrl() {
            return eventSettingsUrl;
        }

        function setEventSettingsUrl(newUrl) {
            if (newUrl) {
                eventSettingsUrl = newUrl;
                // TODO:
                // Save into indexedDB and/or localstorage.
            }
        }

        // Event ID:
        function getEventID() {
            return eventID;
        }

        function setEventID(newID) {
            if (newID) {
                eventID = newID;
                // TODO:
                // Save into indexedDB and/or localstorage.
            }
        }

        // EVENT MAIN DATA:
        function getEventSettingsData() {
            return eventSettingsData;
        }

        function setEventSettingsData(newEventSettingsData) {
            if (newEventSettingsData) {
                eventSettingsData = newEventSettingsData;
                // TODO:
                // Save into indexedDB and/or localstorage.
            }
        }

        // USER ID:
        function getUserID() {
            return userID;
        }

        function setUserID(newUserID) {
            if (newUserID) {
                userID = newUserID;
                // TODO:
                // Save into indexedDB and/or localstorage.
            }
        }

        // USER Info:
        function getLoggedUserInfo() {
            return loggedUserInfo;
        }

        function setLoggedUserInfo(newUserInfo) {
            if (newUserInfo) {
                loggedUserInfo = newUserInfo;
                // TODO:
                // Save into indexedDB and/or localstorage.
            }
        }

        // Event Settings URL:
        function getEventModulesArr() {
            return allEventModules;
        }

        function isModuleAddedIntoEventModulesArr(newModule) {
            if (allEventModules.indexOf(newModule) == -1) {
                return false;
            }
            return true;
        }

        function addNewModuleIntoEventModulesArr(newModule) {
            if (newModule) {
                allEventModules.push(newModule);
            }
        }

        return {
            getEventSettingsUrl: getEventSettingsUrl,
            setEventSettingsUrl: setEventSettingsUrl,
            getEventMenuItemsArr: getEventMenuItemsArr,
            setEventMenuItemsArr: setEventMenuItemsArr,
            getEventSettingsData: getEventSettingsData,
            setEventSettingsData: setEventSettingsData,
            getUserID: getUserID,
            setUserID: setUserID,
            getEventID: getEventID,
            setEventID: setEventID,
            getLoggedUserInfo: getLoggedUserInfo,
            setLoggedUserInfo: setLoggedUserInfo,
            getEventModulesArr: getEventModulesArr,
            isModuleAddedIntoEventModulesArr: isModuleAddedIntoEventModulesArr,
            addNewModuleIntoEventModulesArr: addNewModuleIntoEventModulesArr
        };
    }
]);