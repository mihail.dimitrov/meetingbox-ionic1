angular.module('settings', [])
    // SET UP BROWSER:
    .factory('SetUpBrowser', [
        'AppFunctions',
        'MeetingboxDB',
        function(
            AppFunctions,
            MeetingboxDB) {

            function init() {
                console.log('Init Service -> SetUpBrowser.');
                // Get main settings for current event:
                MeetingboxDB.getEventSetings().then(function(success) {
                    console.log(success);
                    AppFunctions.setUpEventSettings();
                }, function(error) {
                    console.log(error);
                    // TODO:
                    // HAndle with Error Module:
                });
            }

            return {
                init: init
            };
        }
    ]);